mod imp;


use glib::{clone, Object, DateTime};

use adw::subclass::prelude::*;
use adw::prelude::*;
use gtk::{gio, glib, SignalListItemFactory, Button, PositionType, Popover, Calendar, Label, Widget};
use gtk::{ListItem};
use chrono::{Local,Datelike, NaiveDate};

use crate::group_object::GroupObject;
use crate::transaction_object::TransactionObject;



glib::wrapper! {
    pub struct TransactionDialog(ObjectSubclass<imp::TransactionDialog>)
        @extends gtk::Box, gtk::Widget, 
        @implements gio::ActionGroup, gio::ActionMap, gtk::Accessible, gtk::Buildable,
                    gtk::Orientable;
}


impl TransactionDialog {
    pub fn new(addvailable:&bool, source:&i32) -> Self {
        Object::builder().property("addvailable", addvailable).property("source", source).build()
    }

    pub fn setup_object(&self, transaction_object: &TransactionObject) {
        self.imp().description.set_text(&transaction_object.description());
        self.imp().amount.set_text(&transaction_object.amount().to_string());
        self.imp().date.set_subtitle(&transaction_object.date());
        if transaction_object.income() {
            self.imp().income.set_selected(0);
        } else {
            self.imp().income.set_selected(1);
        }
        match self.imp().group.model() {
            Some(groups) => {
                let mut position = 0;
                while let Some(item) = groups.item(position) {
                    let group_object = item
                        .downcast_ref::<GroupObject>()
                        .expect("The object needs to be of type `GroupObject`.");

                    if group_object.title() == transaction_object.group() {
                        self.imp().group.set_selected(position);
                        break;
                    } else {
                        position += 1;
                    }
                }
            }
            None => {},
        }
        let interval = match transaction_object.interval().as_str() {
            "Hằng ngày" => 1,
            "Hằng tuần" => 2,
            "Hằng tháng" => 3,
            "Hằng năm" => 4,
            "Không bao giờ" | _ => 0,
        };
        self.imp().interval.set_selected(interval);
        self.imp().endate.set_subtitle(&transaction_object.enddate());
        self.set_source(transaction_object.source());
    }

    pub fn setup_group(&self, groups: &gio::ListStore) {
        let factory = SignalListItemFactory::new();
        factory.connect_setup(move |_, list_item| {
            // Create label
            let label = Label::new(None);
            let list_item = list_item
                .downcast_ref::<ListItem>()
                .expect("Needs to be ListItem");
            list_item.set_child(Some(&label));

            // Bind `list_item->item->string` to `label->label`
            list_item
                .property_expression("item")
                .chain_property::<GroupObject>("title")
                .bind(&label, "label", Widget::NONE);
        });
        self.imp().group.set_factory(Some(&factory));
        self.imp().group.set_model(Some(groups));
    }

    fn addvailabled(&self) {
        if !self.imp().endate.has_css_class("error") && !self.imp().description.has_css_class("error") && !self.imp().amount.has_css_class("error") && !self.imp().endate.has_css_class("error") {
            self.set_addvailable(true);
        } else {
            self.set_addvailable(false);
        }
        self.emit_by_name::<()>("addvailable", &[&self.addvailable()]);
    }

    pub fn return_object(&self) -> TransactionObject {
        let local_date = Local::now().date_naive();
        let des = self.imp().description.text().to_string();
        let amount;
        match self.imp().amount.text().parse::<f64>() {
            Ok(a) => {
                amount = a;
            }
            Err(_e) => {
                amount = 0.0;
            }
        }
        let income = match self.imp().income.selected() {
            1 => false,
            0 | _ => true,
        };
        let date;
        match self.imp().date.subtitle() {
            Some(date_g) => {
                date = date_g.to_string();
            }
            None => {
                date = local_date.format("%d/%m/%Y").to_string();
            },
        }
        let group;
        match self.imp().group.selected_item().and_downcast::<GroupObject>() {
            Some(group_object) => {
                group = group_object.title();
            }
            None => {
                group = "Chưa có nhóm".to_string();
            },
        }
        
        let interval = match self.imp().interval.selected() {
            1 => "Hằng ngày",
            2 => "Hằng tuần",
            3 => "Hằng tháng",
            4 => "Hằng năm",
            0 | _ => "Không bao giờ",
        };
        let endate;
        match self.imp().endate.subtitle() {
            Some(endate_g) => {
                endate = endate_g.to_string();
            }
            None => {
                endate = "".to_string();
            }
        }

        let done = if interval == "Không bao giờ" {
            true
        } else {
            if endate == "" {
                false
            } else {
                let end = NaiveDate::parse_from_str(&endate, "%d/%m/%Y").unwrap();
                if end >= local_date {
                    false
                } else {
                    true
                }
            }
        };
        return TransactionObject::new(0,self.source(),des, amount, income, group, date, interval.to_string(), endate, done);
    }

    fn setup_call_back(&self) {
        let local_date = Local::now();
        self.imp().date.set_subtitle(&local_date.format("%d/%m/%Y").to_string());
        let date_button = Button::builder().icon_name("calendar-blank-icon").has_frame(false).build();
        let endate_button = Button::builder().icon_name("calendar-blank-icon").has_frame(false).build();
        let clear_button = Button::builder().icon_name("edit-delete-symbolic").has_frame(false).build();
        self.imp().date.add_suffix(&date_button);
        self.imp().endate.add_suffix(&endate_button);
        self.imp().endate.add_suffix(&clear_button);
        self.imp().date.set_activatable_widget(Some(&date_button));
        self.imp().endate.set_activatable_widget(Some(&endate_button));
        self.imp().endate.set_sensitive(false);
        clear_button
            .connect_clicked(clone!(@weak self as dialog => move |_| {
                dialog.imp().endate.set_subtitle("");
                dialog.imp().endate.remove_css_class("error");
                dialog.addvailabled();
            })
        );

        date_button
            .connect_clicked(clone!(@weak self as dialog => move |_| {
                let pop = Popover::builder().build();
                let calendar = Calendar::builder().build();
                if dialog.imp().date.subtitle().unwrap() != "" {
                    let date = chrono::NaiveDate::parse_from_str(&dialog.imp().date.subtitle().unwrap(), "%d/%m/%Y").unwrap();
                    let a = &DateTime::from_local(date.year(), date.month().try_into().unwrap(), date.day().try_into().unwrap(), 0, 0, 0.0).expect("DateTime");
                    calendar.select_day(a);
                }
                pop.set_child(Some(&calendar));
                pop.set_position(PositionType::Bottom);
                pop.set_pointing_to(Some(&dialog.imp().date.allocation()));
                pop.set_parent(&dialog.imp().date.child().unwrap());
                pop.popup();
                calendar.connect_day_selected(clone! (@weak dialog, @weak calendar, @weak pop => move|_| {
                    let date = calendar.date();
                    let text = format!("{}/{}/{}", date.day_of_month(), date.month(), date.year());
                    dialog.imp().date.set_subtitle(&text);
                    if dialog.imp().endate.subtitle().unwrap() != "" {
                        let dates = chrono::NaiveDate::parse_from_str(&dialog.imp().date.subtitle().unwrap(), "%d/%m/%Y").unwrap();
                        let daten = chrono::NaiveDate::parse_from_str(&dialog.imp().endate.subtitle().unwrap(), "%d/%m/%Y").unwrap();
                        if daten < dates {
                            dialog.imp().endate.add_css_class("error")
                        } else {
                            dialog.imp().endate.remove_css_class("error")
                        }
                    }
                    dialog.addvailabled();
                    pop.hide();
                }));
            })
        );
        endate_button
            .connect_clicked(clone!(@weak self as dialog => move |_| {
                let pop = Popover::builder().build();
                let calendar = Calendar::builder().build();
                if dialog.imp().endate.subtitle().unwrap() != "" {
                    let date = chrono::NaiveDate::parse_from_str(&dialog.imp().endate.subtitle().unwrap(), "%d/%m/%Y").unwrap();
                    let a = &DateTime::from_local(date.year(), date.month().try_into().unwrap(), date.day().try_into().unwrap(), 0, 0, 0.0).expect("DateTime");
                    calendar.select_day(a);
                } else {
                    dialog.imp().endate.set_subtitle(&local_date.format("%d/%m/%Y").to_string());
                }
                pop.set_child(Some(&calendar));
                pop.set_position(PositionType::Bottom);
                pop.set_pointing_to(Some(&dialog.imp().endate.allocation()));
                pop.set_parent(&dialog.imp().endate.child().unwrap());
                pop.popup();
                calendar.connect_day_selected(clone! (@weak dialog, @weak calendar, @weak pop => move|_| {
                    let date = calendar.date();
                    let text = format!("{}/{}/{}", date.day_of_month(), date.month(), date.year());
                    dialog.imp().endate.set_subtitle(&text);
                    let daten = chrono::NaiveDate::parse_from_str(&dialog.imp().endate.subtitle().unwrap(), "%d/%m/%Y").unwrap();
                    let dates = chrono::NaiveDate::parse_from_str(&dialog.imp().date.subtitle().unwrap(), "%d/%m/%Y").unwrap();
                    if daten < dates {
                        dialog.imp().endate.add_css_class("error")
                    } else {
                        dialog.imp().endate.remove_css_class("error")
                    }
                    dialog.addvailabled();
                    pop.hide();
                }));
            })
        );

        self.imp().interval.connect_selected_item_notify(clone!(@weak self as wallet,  => move |_| {
            let pos = wallet.imp().interval.selected();
            match pos {
                1 | 2 | 3 | 4 => {wallet.imp().endate.set_sensitive(true);},
                0 | _ => {wallet.imp().endate.set_sensitive(false);},
            }
        }));

        self.imp().description.connect_changed(clone!(@weak self as dialog => move |entry| {
            let text = entry.text();
            let empty = text.is_empty();
            let amount = dialog.imp().amount.text();
            let mut a = false;

            if let Ok(val) = amount.parse::<f64>() {
                        if val > 0.0 {
                            a = true;
                        } else {
                            a = false;
                        }
                    }

            if empty {
                entry.add_css_class("error");
                dialog.addvailabled();
            } else {
                entry.remove_css_class("error");
                if a {dialog.addvailabled()};
            }
        }));

        self.imp().amount.connect_changed(clone!(@weak self as dialog => move |entry| {
            let amount = entry.text();
            let text = dialog.imp().description.text();
            let empty = text.is_empty();
            let mut a = false;

            if let Ok(val) = amount.parse::<f64>() {
                        if val > 0.0 {
                            a = true;
                        } else {
                            a = false;
                        }
                    }

            if !a {
                entry.add_css_class("error");
                dialog.addvailabled();
            } else {
                entry.remove_css_class("error");
                if !empty {dialog.addvailabled()};
            }
        }));
    }
}