use std::cell::RefCell;
use adw::{ComboRow, ActionRow, EntryRow};
use gtk::glib::{ParamSpec, Value, Properties};
use gtk::glib::once_cell::sync::Lazy;
use gtk::glib::subclass::Signal;
use adw::{prelude::*, subclass::prelude::*};
use gtk::{glib, CompositeTemplate};




#[derive(Default, CompositeTemplate, Properties)]
#[template(resource = "/org/gtk_rs/example/transaction_dialog.ui")]
#[properties(wrapper_type = super::TransactionDialog)]
pub struct TransactionDialog {
    #[template_child]
    pub description: TemplateChild<EntryRow>,
    #[template_child]
    pub amount: TemplateChild<EntryRow>,
    #[template_child]
    pub income: TemplateChild<ComboRow>,
    #[template_child]
    pub group: TemplateChild<ComboRow>,
    #[template_child]
    pub interval: TemplateChild<ComboRow>,
   
    #[template_child]
    pub date: TemplateChild<ActionRow>,
    #[template_child]
    pub endate: TemplateChild<ActionRow>,
    
    #[property(get, set)]
    pub addvailable: RefCell<bool>,
    #[property(get, set)]
    pub source: RefCell<i32>,
    
}

#[glib::object_subclass]
impl ObjectSubclass for TransactionDialog {
    // `NAME` needs to match `class` attribute of template
    const NAME: &'static str = "TransactionDialog";
    type Type = super::TransactionDialog;
    type ParentType = gtk::Box;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
        obj.init_template();
    }
}




impl ObjectImpl for TransactionDialog {
    fn properties() -> &'static [ParamSpec] {
        Self::derived_properties()
    }

    fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
        self.derived_set_property(id, value, pspec)
    }

    fn property(&self, id: usize, pspec: &ParamSpec) -> Value {
        self.derived_property(id, pspec)
    }
    // Call "constructed" on parent
    fn constructed(&self) {
        // Call "constructed" on parent
        self.parent_constructed();
        self.addvailable.set(false);

        let obj = self.obj();
        obj.setup_call_back();
    }


    fn signals() -> &'static [Signal] {
        static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
            vec![Signal::builder("addvailable")
                .param_types([bool::static_type()])
                .build()]
        });
        SIGNALS.as_ref()
    }
}

impl WidgetImpl for TransactionDialog {}

impl BoxImpl for TransactionDialog {}
