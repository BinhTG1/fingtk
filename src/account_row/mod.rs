mod imp;

use glib::Object;
use gtk::{glib};

glib::wrapper! {
    pub struct AccountObject(ObjectSubclass<imp::AccountObject>);
}

// ANCHOR: impl
impl AccountObject {
    pub fn new(name: &str, path:&str) -> Self {
        Object::builder()
            .property("name", name)
            .property("path", path)
            .build()
    }

}
// ANCHOR_END: impl

// ANCHOR: collection_data
#[derive(Default, Clone)]
pub struct AccountData {
    pub name: String,
    pub path: String,
}
// ANCHOR_END: collection_data