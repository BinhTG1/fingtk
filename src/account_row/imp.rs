use std::cell::RefCell;

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::{ParamSpec, Properties, Value};
use gtk::{glib};

// ANCHOR: collection_object
// Object holding the state
#[derive(Properties, Default)]
#[properties(wrapper_type = super::AccountObject)]
pub struct AccountObject {
    #[property(get, set)]
    pub name: RefCell<String>,
    #[property(get, set)]
    pub path: RefCell<String>,
}

// The central trait for subclassing a GObject
#[glib::object_subclass]
impl ObjectSubclass for AccountObject {
    const NAME: &'static str = "AccountObject";
    type Type = super::AccountObject;
}
// ANCHOR_END: collection_object

// Trait shared by all GObjects
impl ObjectImpl for AccountObject {
    fn properties() -> &'static [ParamSpec] {
        Self::derived_properties()
    }

    fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
        self.derived_set_property(id, value, pspec)
    }

    fn property(&self, id: usize, pspec: &ParamSpec) -> Value {
        self.derived_property(id, pspec)
    }
}