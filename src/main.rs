mod window;
mod transaction_object;
mod transaction_dialog;
mod wallet_tab;
mod account_collection;
mod group_object;
mod utils;
mod account_row;

use adw::prelude::*;
use gtk::{gio, glib, IconTheme, gdk::Display};
use window::Window;



const APP_ID: &str = "org.gtk_rs.fingtk";

fn main() -> glib::ExitCode {
    // Register and include resources
    gio::resources_register_include!("fingtk.gresource")
        .expect("Failed to register resources.");

    // Create a new application
    let app = adw::Application::builder().application_id(APP_ID).build();
    

    
    // Connect to "activate" signal of `app`
    app.connect_activate(build_ui);
    
    // Run the application
    app.run()
}
fn build_ui(app: &adw::Application) {
    // Create new window and present it
    let window = Window::new(app);
    let display = Display::default().unwrap();
    let icon_theme = IconTheme::for_display(&display);
    icon_theme.add_search_path(r"src\resources\icon\places");
    icon_theme.add_search_path(r"src\resources\icon\actions");
    icon_theme.add_search_path(r"src\resources\icon\categories");
    icon_theme.add_search_path(r"src\resources\icon\status");
    icon_theme.set_theme_name(Some("try"));
    
    // let style = StyleManager::default();
    // style.set_color_scheme(adw::ColorScheme::PreferDark);
   
    // unsafe { adw_style_manager_set_color_scheme(adw_style_manager_get_default(), ADW_COLOR_SCHEME_PREFER_DARK) };

    window.present();
}
