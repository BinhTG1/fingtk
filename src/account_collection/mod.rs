mod imp;

use adw::prelude::{ListModelExtManual, *};
use adw::subclass::prelude::*;
use glib::Object;
use gtk::{gio, glib};
use serde::{Deserialize, Serialize};

use crate::group_object::{GroupData, GroupObject};
use crate::transaction_object::{TransactionData, TransactionObject};

glib::wrapper! {
    pub struct CollectionObject(ObjectSubclass<imp::CollectionObject>);
}

// ANCHOR: impl
impl CollectionObject {
    pub fn new(name: &str,currency: &str, total: &f64, expense: &f64, income: &f64, groups: gio::ListStore, transactions: gio::ListStore) -> Self {
        Object::builder()
            .property("name", name)
            .property("currency", currency)
            .property("total", total)
            .property("expense", expense)
            .property("income", income)
            .property("groups", groups)
            .property("transactions", transactions)
            .build()
    }

    pub fn to_collection_data(&self) -> CollectionData {
        let name = self.imp().name.borrow().clone();
        let currency = self.imp().currency.borrow().clone();
        let total = self.imp().total.borrow().clone();
        let expense = self.imp().expense.borrow().clone();
        let income = self.imp().income.borrow().clone();
        let groups_data = self
            .groups()
            .snapshot()
            .iter()
            .filter_map(Cast::downcast_ref::<GroupObject>)
            .map(GroupObject::group_data)
            .collect();
        let transactions_data = self
            .transactions()
            .snapshot()
            .iter()
            .filter_map(Cast::downcast_ref::<TransactionObject>)
            .map(TransactionObject::transaction_data)
            .collect();

        CollectionData { name, currency, total, expense, income, groups_data, transactions_data }
    }

    pub fn from_collection_data(collection_data: CollectionData) -> Self {
        let name = collection_data.name;
        let currency = collection_data.currency;
        let total = collection_data.total;
        let expense = collection_data.expense;
        let income = collection_data.income;
        let groups_to_extend: Vec<GroupObject> = collection_data
            .groups_data
            .into_iter()
            .map(GroupObject::from_group_data)
            .collect();

        let groups = gio::ListStore::new(GroupObject::static_type());
        groups.extend_from_slice(&groups_to_extend);
        let transactions_to_extend: Vec<TransactionObject> = collection_data
            .transactions_data
            .into_iter()
            .map(TransactionObject::from_transaction_data)
            .collect();

        let transactions = gio::ListStore::new(TransactionObject::static_type());
        transactions.extend_from_slice(&transactions_to_extend);

        Self::new(&name, &currency, &total, &expense, &income, groups, transactions)
    }



}
// ANCHOR_END: impl

// ANCHOR: collection_data
#[derive(Default, Clone, Serialize, Deserialize)]
pub struct CollectionData {
    pub name: String,
    pub currency: String,
    pub total: f64,
    pub expense: f64,
    pub income: f64,
    pub groups_data: Vec<GroupData>,
    pub transactions_data: Vec<TransactionData>,
}
// ANCHOR_END: collection_data