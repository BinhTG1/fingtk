use std::cell::RefCell;

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::{ParamSpec, Properties, Value};
use gtk::{gio, glib};
use once_cell::sync::OnceCell;

// ANCHOR: collection_object
// Object holding the state
#[derive(Properties, Default)]
#[properties(wrapper_type = super::CollectionObject)]
pub struct CollectionObject {
    #[property(get, set)]
    pub name: RefCell<String>,
    #[property(get, set)]
    pub currency: RefCell<String>,
    #[property(get, set)]
    pub income: RefCell<f64>,
    #[property(get, set)]
    pub expense: RefCell<f64>,
    #[property(get, set)]
    pub total: RefCell<f64>,
    #[property(get, set)]
    pub groups: OnceCell<gio::ListStore>,
    #[property(get, set)]
    pub transactions: OnceCell<gio::ListStore>,
}

// The central trait for subclassing a GObject
#[glib::object_subclass]
impl ObjectSubclass for CollectionObject {
    const NAME: &'static str = "WalletCollectionObject";
    type Type = super::CollectionObject;
}
// ANCHOR_END: collection_object

// Trait shared by all GObjects
impl ObjectImpl for CollectionObject {
    fn properties() -> &'static [ParamSpec] {
        Self::derived_properties()
    }

    fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
        self.derived_set_property(id, value, pspec)
    }

    fn property(&self, id: usize, pspec: &ParamSpec) -> Value {
        self.derived_property(id, pspec)
    }
}