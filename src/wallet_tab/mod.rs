
mod imp;
use std::fs::File;
use chrono::Days;
use chrono::Local;
use chrono::Months;
use chrono::NaiveDate;
use chrono::Datelike;
use glib::{clone, Object, DateTime};

use gtk::glib::{closure_local, BindingFlags};
use adw::subclass::prelude::*;
use adw::{prelude::*, ActionRow, ButtonContent, EntryRow, MessageDialog, ResponseAppearance};
use gtk::{gio, glib, NoSelection, CheckButton, Align, Button, CustomFilter, PositionType, Popover, Calendar, Label, FilterListModel};



use crate::account_collection::{CollectionObject, CollectionData};
use crate::group_object::GroupObject;
use crate::transaction_dialog::TransactionDialog;
use crate::transaction_object::TransactionObject;
use crate::utils::data_path;
use crate::window::Window;

glib::wrapper! {
    pub struct Wallet(ObjectSubclass<imp::Wallet>)
    @extends gtk::Box, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl Wallet {
    pub fn new() -> Self {
        // Create new window
        Object::builder().build()
    }

    pub fn set_col(&self, collection:&CollectionObject) {
        self.imp()
            .collection
            .set(collection.clone())
            .expect("Could not set collection");
    }

    pub fn restore_data(&self, name:&str) {
        if let Ok(file) = File::open(data_path(&name.to_string())) {
            // Deserialize data from file to vector
            let backup_data: CollectionData = serde_json::from_reader(file)
                .expect(
                    "It should be possible to read `backup_data` from the json file.",
                );
            let collection = CollectionObject::from_collection_data(backup_data);
            self.set_col(&collection);
        }
    }
    
    fn collection(&self) -> CollectionObject {
        self.imp()
            .collection
            .get()
            .expect("`collection` should be set in `set_col")
            .clone()
    }

    fn transactions(&self) -> gio::ListStore {
        // Get state
        self.collection().transactions()
    }

    fn groups(&self) ->gio::ListStore {
        self.collection().groups()
    }

    fn set_filter(&self) {
        self.imp()
            .current_filter_model
            .borrow()
            .clone()
            .expect("`current_filter_model` should be set in `set_col`.")
            .set_filter(self.filter().as_ref());
    }

    pub fn setup_overview(&self) {
        let overview_label = self.imp().overview.get();
        let total_label = self.imp().total_row.get();
        let income_label= self.imp().income_row.get();
        let expense_label = self.imp().expense_row.get();
        let expense_check = self.imp().expense_check.get();
        let income_check = self.imp().income_check.get();
        let currency = self.collection().currency();
        let currency1 = self.collection().currency();
        let currency2 = self.collection().currency();
        self.set_income(true);
        self.set_expense(true);
        self.bind_property("income", &income_check, "active").flags(BindingFlags::SYNC_CREATE|BindingFlags::BIDIRECTIONAL)
        .build();
        self.bind_property("expense", &expense_check, "active").flags(BindingFlags::SYNC_CREATE|BindingFlags::BIDIRECTIONAL)
        .build();
        self.collection().bind_property("name", &overview_label, "title")
        .transform_to(move |_, name: String| {
            let result = format!("{}{}", "Tổng quan tài khoản ", name);
            Some(result.to_value())
        })
        .flags(BindingFlags::SYNC_CREATE)
        .build();

        self.collection().bind_property("total", &total_label, "title")
        .transform_to(move |_, total: f64| {
            let str = format!("{:.8}", total).trim_end_matches('0').trim_end_matches('.').to_string();
            let result = format!("{}{} {}", "Số dư tổng: ", str, currency);
            Some(result.to_value())
        })
        .flags(BindingFlags::SYNC_CREATE)
        .build();

        self.collection().bind_property("income", &income_label, "title")
        .transform_to(move |_, income: f64| {
            let str = format!("{:.8}", income).trim_end_matches('0').trim_end_matches('.').to_string();
            let result = format!("{}{} {}", "Thu nhập: ", str, currency1);
            Some(result.to_value())
        })
        .flags(BindingFlags::SYNC_CREATE)
        .build();

        self.collection().bind_property("expense", &expense_label, "title")
        .transform_to(move |_, expense: f64| {
            let str = format!("{:.8}", expense).trim_end_matches('0').trim_end_matches('.').to_string();
            let result = format!("{}{} {}", "Chi tiêu: ", str, currency2);
            Some(result.to_value())
        })
        .flags(BindingFlags::SYNC_CREATE)
        .build();

        self.collection().bind_property("currency", &total_label, "title")
        .flags(BindingFlags::DEFAULT)
        .build();
    }
    
    pub fn setup_transaction(&self) {
        self.imp().transaction_list.set_max_children_per_line(4);
        self.imp().transaction_list.set_min_children_per_line(1);
        let transactions = self.transactions();
        let filter_model = FilterListModel::new(Some(transactions.clone()), self.filter());
        let selection_model = NoSelection::new(Some(filter_model.clone()));
        self.imp().transaction_list.bind_model(
            Some(&selection_model),
            clone!(@weak self as wallet => @default-panic, move |obj| {
                let transaction_object = obj
                    .downcast_ref()
                    .expect("The object should be of type `transaction_object`.");

                
                let row = wallet.create_transaction_row(transaction_object);
                row.upcast()
            }),
        );
        if transactions.n_items() != 0 {
            self.search_for_needed_update();
        }
        self.imp().current_filter_model.replace(Some(filter_model));
        
    }

    pub fn setup_group(&self) {
        let groups = self.groups();
        self.imp().group_scroll.set_propagate_natural_height(true);
        self.imp().group_scroll.set_max_content_height(5 * 54);
        let selection_model = NoSelection::new(Some(groups.clone()));
        self.imp().group_list.bind_model(
            Some(&selection_model),
            clone!(@weak self as wallet => @default-panic, move |obj| {
                let group_object = obj
                    .downcast_ref()
                    .expect("The object should be of type `GroupObject`.");
                let row = wallet.create_group_row(group_object);
                row.upcast()
            }),
        );
        
    }

    pub fn setup_date_filter(&self) {
        self.imp().date2.set_visible(false);
        self.imp().date.set_visible(false);
        self.imp().date_type.connect_selected_item_notify(clone!(@weak self as wallet,  => move |_| {
            let pos = wallet.imp().date_type.selected();
            match pos {
                1 | 2 | 3 => {wallet.imp().date2.set_visible(false);
                        wallet.imp().date.set_title("Chọn ngày");
                        wallet.imp().date.set_visible(true);
                    wallet.set_filter()},
                4 => {wallet.imp().date2.set_visible(true);
                        wallet.imp().date.set_title("Từ ngày");
                        wallet.imp().date.set_visible(true);
                        wallet.set_filter()},
                0 | _ => {wallet.imp().date2.set_visible(false);
                        wallet.imp().date.set_visible(false);
                        wallet.set_filter()},
            }
        }));
        
    }

    fn create_group_row(&self, group_object: &GroupObject) -> ActionRow {
        let row = ActionRow::builder()
            .build();
        let check_button = CheckButton::builder()
            .valign(Align::Center)
            .can_focus(false)
            .build();
        // Bind properties
        group_object
            .bind_property("checked", &check_button, "active")
            .flags(BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL)
            .build();
        group_object
            .bind_property("title", &row, "title")
            .flags(BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL)
            .build();
        
        if self.groups().n_items() <= 5 {
            self.imp().group_scroll.set_min_content_height(54 * (self.groups().n_items()) as i32);
        }
        if group_object.is_checked() {
            self.imp().checked.borrow_mut().push(group_object.title());
        }
        group_object
        .bind_property("checked", &check_button, "active")
        .flags(BindingFlags::SYNC_CREATE | BindingFlags::BIDIRECTIONAL)
        .build();
        check_button.connect_toggled(clone!(@weak self as wallet, @weak group_object => move |_| {
            if group_object.is_checked() {
                if !wallet.imp().checked.borrow().contains(&group_object.title()) {
                    wallet.imp().checked.borrow_mut().push(group_object.title());
                }
            } else {
                if wallet.imp().checked.borrow().contains(&group_object.title()) {
                    wallet.imp().checked.borrow_mut().retain(|x| x != &group_object.title());
                }
            }
            wallet.set_filter();
        }));
        row.add_prefix(&check_button);
        if group_object.title() == "Chưa có nhóm" {
            return row;
        }
        let edit_button = Button::builder()
            .valign(Align::Center)
            .can_focus(false)
            .child(&ButtonContent::builder().icon_name("document-edit-symbolic").build())
            .has_frame(false)
            .build();
        
        let delete_button = Button::builder()
            .valign(Align::Center)
            .can_focus(false)
            .child(&ButtonContent::builder().icon_name("edit-delete-symbolic").build())
            .has_frame(false)
            .build();
        
        row.add_suffix(&edit_button);
        row.add_suffix(&delete_button);

        edit_button.connect_clicked(clone!(@weak self as wallet, @weak group_object => move |_| {
            let entry_group_name = EntryRow::builder().title("Tên nhóm mới").activates_default(true).build();
            let cancel_response = "cancel";
            let edit_response = "edit";
            
            let dialog = MessageDialog::builder()
            .heading("Sửa Tên Nhóm")
            .modal(true)
            .transient_for(&wallet.root()
            .unwrap()
            .downcast::<Window>()
            .unwrap())
            .destroy_with_parent(true)
            .close_response(cancel_response)
            .default_response(edit_response)
            .extra_child(&entry_group_name)
            .build();
            dialog.add_responses(&[(cancel_response, "Hủy"), (edit_response, "Sửa")]);
            dialog.set_response_enabled(edit_response, false);
            entry_group_name.connect_changed(clone!(@weak dialog, @weak wallet => move |entry| {
                let text = entry.text();
                let empty = text.is_empty();
                
                dialog.set_response_enabled(edit_response, !empty&&wallet.check_dup_group(text.to_string()));
    
            }));
            dialog.set_response_appearance(edit_response, ResponseAppearance::Suggested);
            dialog.connect_response(
                None,
                clone!(@weak wallet, @weak entry_group_name, @weak group_object  => move |dialog, response| {
                    // Destroy dialog
                    dialog.destroy();
    
                    // Return if the user chose a response different than `create_response`
                    if response != edit_response {
                        return;
                    }
                    let name = entry_group_name.text().to_string();
                    let mut vec = wallet.imp().checked.borrow_mut();
                    vec.retain(|s| 
                        s != &group_object.title()
                    );
                    if group_object.is_checked() {
                        vec.push(name.clone());
                    }
                    let mut position = 0;

                    while let Some(item) = wallet.transactions().item(position) {
                        // Get `TaskObject` from `glib::Object`
                        let transaction_object = item
                            .downcast_ref::<TransactionObject>()
                            .expect("The object needs to be of type `TransactionObject`.");

                        if transaction_object.group() == group_object.title() {
                            transaction_object.set_group(name.clone());
                        } else {
                            position += 1;
                        }
                    }
                    group_object.set_title(name);
                    
                }),
            );
            dialog.present();
        }));
        delete_button.connect_clicked(clone!(@weak self as wallet, @weak group_object => move |_| {
            let cancel_response = "cancel";
            let delete_response = "delete";
            let edit_response = "edit";
            
            let dialog = MessageDialog::builder()
            .heading("Xóa nhóm")
            .body("Xin hãy lựa chọn phương án xóa.")
            .modal(true)
            .transient_for(&wallet.root()
            .unwrap()
            .downcast::<Window>()
            .unwrap())
            .destroy_with_parent(true)
            .close_response(cancel_response)
            .default_response(edit_response)
            .build();
            dialog.add_responses(&[(cancel_response, "Hủy"),(delete_response, "Xóa mọi giao dịch thuộc nhóm này"), (edit_response, "Chuyển mọi giao dịch thuộc nhóm này về chưa có nhóm")]);
            dialog.set_response_appearance(delete_response, ResponseAppearance::Destructive);
            dialog.set_response_appearance(edit_response, ResponseAppearance::Destructive);
            dialog.connect_response(
                None,
                clone!(@weak wallet, @weak group_object => move |_dialog, response| {
                    // Return if the user chose a response different than `create_response`
                    if response == cancel_response {
                        return;
                    }
                    if response == delete_response {
                        wallet.delete_group(group_object.title(), true);
                        return;
                    }
                    wallet.delete_group(group_object.title(), false)
                }));
            dialog.present();
        }));

        row
    }

    fn create_transaction_row(&self, transaction_object: &TransactionObject) -> ActionRow {
        let delete_button = Button::builder()
            .valign(Align::Center)
            .can_focus(false)
            .icon_name("edit-delete")
            .has_frame(false)
            .build();
        let edit_button = Button::builder()
            .valign(Align::Center)
            .can_focus(false)
            .icon_name("edit-copy-symbolic")
            .has_frame(false)
            .build();

        delete_button.connect_clicked(clone!(@weak self as wallet, @weak transaction_object => move |_| {
                let cancel_response = "cancel";
                let delete_response = "delete";
                
                let dialog = MessageDialog::builder()
                .heading("Xoá giao dịch")
                .body("Lưu ý nếu giao dịch được tạo ra bởi giao dịch lặp lại khi thay đổi giao dịch gốc sẽ sinh lại toàn bộ giao dịch đã được xóa.")
                .modal(true)
                .transient_for(&wallet.root()
                .unwrap()
                .downcast::<Window>()
                .unwrap())
                .destroy_with_parent(true)
                .close_response(cancel_response)
                .default_response(delete_response)
                .build();
                
                dialog.add_responses(&[(cancel_response, "Hủy"), (delete_response, "Xóa")]);
              
                dialog.set_response_appearance(delete_response, ResponseAppearance::Destructive);
                dialog.connect_response(
                    None,
                    clone!(@weak wallet, @weak transaction_object=> move |dialog, response| {
                        // Destroy dialog
                        dialog.destroy();
                        
                        // Return if the user chose a response different than `create_response`
                        if response == cancel_response {
                            return;
                        }
                        if transaction_object.interval() != "Không bao giờ" {
                            wallet.delete_interval(transaction_object.id().try_into().unwrap())
                        }
                        wallet.delete_transaction(transaction_object.id());
                    }),
                );
                dialog.present();
        }));
        
        edit_button.connect_clicked(clone!(@weak self as wallet, @weak transaction_object => move |_| {
            let trans = TransactionDialog::new(&true, &transaction_object.source());
            let cancel_response = "cancel";
            let copy_response = "copy";
            let edit_response = "edit";
            
            let dialog = MessageDialog::builder()
            .heading("Sửa Giao Dịch")
            .modal(true)
            .transient_for(&wallet.root()
            .unwrap()
            .downcast::<Window>()
            .unwrap())
            .destroy_with_parent(true)
            .close_response(cancel_response)
            .default_response(edit_response)
            .extra_child(&trans)
            .build();
            trans.setup_group(&wallet.groups());
            trans.setup_object(&transaction_object);
            dialog.add_responses(&[(cancel_response, "Hủy"),(copy_response, "Tạo bản sao"), (edit_response, "Sửa")]);
            dialog.set_response_appearance(copy_response, ResponseAppearance::Suggested);
            dialog.set_response_appearance(edit_response, ResponseAppearance::Suggested);
            dialog.connect_response(
                None,
                clone!(@weak wallet, @weak trans, @weak transaction_object => move |dialog, response| {
                    // Destroy dialog
                    dialog.destroy();
                    
                    // Return if the user chose a response different than `create_response`
                    if response == cancel_response {
                        return;
                    }
                    if response == copy_response {
                        let o = wallet.transactions().item(wallet.transactions().n_items()-1).and_downcast::<TransactionObject>().expect("TransactionObject");
                        let id = o.id() + 1;
                        let copy = TransactionObject::from_transaction_data(transaction_object.transaction_data());
                        copy.set_id(id);
                        wallet.new_transaction(copy);
                        return;
                    }
                    let i = transaction_object.income();
                    

                    let t = trans.return_object();
                    if t.income() != i {
                        if i {
                            wallet.collection().set_total(wallet.collection().total() - transaction_object.amount() - t.amount());
                            wallet.collection().set_income(wallet.collection().income() - transaction_object.amount());
                            wallet.collection().set_expense(wallet.collection().expense() + t.amount());
                        } else {
                            wallet.collection().set_total(wallet.collection().total() + transaction_object.amount() + t.amount());
                            wallet.collection().set_income(wallet.collection().income() + t.amount());
                            wallet.collection().set_expense(wallet.collection().expense() - transaction_object.amount());
                        }
                    } else {
                        if i {
                            wallet.collection().set_total(wallet.collection().total() - transaction_object.amount() + t.amount());
                            wallet.collection().set_income(wallet.collection().income() - transaction_object.amount() + t.amount());
                        } else {
                            wallet.collection().set_total(wallet.collection().total() + transaction_object.amount() - t.amount());
                            wallet.collection().set_expense(wallet.collection().expense() - transaction_object.amount()+ t.amount());
                        }
                    }
                    transaction_object.set_description(t.description());
                    transaction_object.set_amount(t.amount());
                    transaction_object.set_date(t.date());
                    transaction_object.set_income(t.income());
                    
                    transaction_object.set_group(t.group());
                    transaction_object.set_enddate(t.enddate());
                    
                    if transaction_object.interval() != "Không bao giờ" {
                        wallet.delete_interval(transaction_object.id().try_into().unwrap());
                    }
                    
                    transaction_object.set_interval(t.interval());
                    

                    if transaction_object.interval() != "Không bao giờ" {
                        let date_now = Local::now().date_naive();
                        let date_end = if transaction_object.enddate() != "" {
                            NaiveDate::parse_from_str(&transaction_object.enddate(), "%d/%m/%Y").unwrap()
                        } else {
                            NaiveDate::MAX
                        };
                        if date_now >= date_end {
                            transaction_object.set_done(true);
                            wallet.setup_interval(&transaction_object, &date_end);
                        } else {
                            transaction_object.set_done(false);
                            wallet.setup_interval(&transaction_object, &date_now);
                        }
                    } else {
                        transaction_object.set_done(true);
                    }
                    
                    transaction_object.set_interval(t.interval());
                    
                    
                    wallet.set_filter();
                }),
            );
            dialog.present();
            trans.connect_closure(
                "addvailable", false,
                closure_local!(move |_trans: TransactionDialog, add: bool| {
                    dialog.set_response_enabled(edit_response, add);
                }));
        }));

        let row = ActionRow::builder()
            .activatable_widget(&delete_button)
            .height_request(54)
            .build();
        
        row.add_css_class("frame");
        row.add_css_class("background");
        let inner_row = ActionRow::builder()
            .build();
        
         let id = Label::builder().halign(gtk::Align::Center).margin_start(5).build();
         let income_label = Label::builder().halign(gtk::Align::Center).margin_start(5).build();
         let amount_label = Label::builder().halign(gtk::Align::Center).build();
        // let interval = Label::builder().halign(gtk::Align::Center).build();
        transaction_object.bind_property("income", &income_label, "label")
        .transform_to(|_, income: bool| {
            if income {
                Some("+".to_value())
            } else {
                Some("-".to_value())
            }
        })
        .flags(BindingFlags::SYNC_CREATE)
        .build();
        transaction_object.bind_property("amount", &amount_label, "label")
        .transform_to(move |_, amount: f64| {
            let str = format!("{:.8}", amount).trim_end_matches('0').trim_end_matches('.').to_string();
            Some(str.to_value())
        })
        .flags(BindingFlags::SYNC_CREATE)
        .build();

        

        transaction_object.bind_property("id", &id, "label")
        .flags(BindingFlags::SYNC_CREATE)
        .build();

        // Bind properties
        transaction_object
            .bind_property("date", &row, "subtitle")
            .flags(BindingFlags::SYNC_CREATE)
            .build();

        transaction_object
            .bind_property("description", &row, "title")
            .transform_to(move |_, string: &str| {
                if string.len() > 22 {
                    let mut shortened = String::from(&string[..19]);
                    shortened.push_str("...");
                    Some(shortened.to_value())
                } else {
                    Some(string.to_value())
                }
                
            })
            .flags(BindingFlags::SYNC_CREATE)
            .build();

        row.add_prefix(&id);
        row.add_suffix(&income_label);
        row.add_suffix(&amount_label);
        if transaction_object.source() != -1 {
            let edit = Button::builder()
                .valign(Align::Center)
                .can_focus(false)
                .icon_name("document-edit-symbolic")
                .has_frame(false)
                .build();
            edit.connect_clicked(clone!(@weak self as wallet, @weak transaction_object => move |_| {
                let entry = EntryRow::builder().title("Lượng tiền mới").activates_default(true).build();
                let cancel_response = "cancel";
                let edit_response = "edit";
                
                let dialog = MessageDialog::builder()
                .heading("Sửa lượng tiền")
                .body("Lưu ý mọi thay đổi trên các giao dịch được sinh ra từ các giao dịch lặp lại sẽ biến mất khi giao dịch gốc thay đổi.")
                .modal(true)
                .transient_for(&wallet.root()
                .unwrap()
                .downcast::<Window>()
                .unwrap())
                .destroy_with_parent(true)
                .close_response(cancel_response)
                .default_response(edit_response)
                .extra_child(&entry)
                .build();
                
                dialog.add_responses(&[(cancel_response, "Hủy"), (edit_response, "Sửa")]);
              
                dialog.set_response_appearance(edit_response, ResponseAppearance::Suggested);
                entry.connect_changed(clone!(@weak dialog => move |entry| {
                    let amount = entry.text();
                    let mut a = false;
        
                    if let Ok(val) = amount.parse::<f64>() {
                                if val > 0.0 {
                                    a = true;
                                } else {
                                    a = false;
                                }
                            }
        
                    if !a {
                        entry.add_css_class("error");
                        dialog.set_response_enabled(edit_response, false);
                    } else {
                        entry.remove_css_class("error");
                        dialog.set_response_enabled(edit_response, true);
                    }
                }));
                dialog.connect_response(
                    None,
                    clone!(@weak wallet, @weak transaction_object, @weak entry => move |dialog, response| {
                        // Destroy dialog
                        dialog.destroy();
                        
                        // Return if the user chose a response different than `create_response`
                        if response == cancel_response {
                            return;
                        }
                        let i = transaction_object.income();
                        let mut amount_new = 0.0;
                        if let Ok(val) = entry.text().parse::<f64>() {
                            if val > 0.0 {
                                amount_new = val;
                            } else {
                                amount_new = 0.0;
                            }
                        }
                        if i {
                            wallet.collection().set_total(wallet.collection().total() - transaction_object.amount() + amount_new);
                            wallet.collection().set_income(wallet.collection().income() - transaction_object.amount()+ amount_new);
                            
                        } else {
                            wallet.collection().set_total(wallet.collection().total() + transaction_object.amount() - amount_new);
                            wallet.collection().set_expense(wallet.collection().expense() - transaction_object.amount()+ amount_new);
                        }
                        transaction_object.set_amount(amount_new);
                        wallet.set_filter();
                    }),
                );
                dialog.present();
            }));
            row.add_suffix(&edit);
            row.add_suffix(&delete_button);
            return row;
        }
        row.add_suffix(&inner_row);
        transaction_object.bind_property("interval", &inner_row, "subtitle")
        .transform_to(|_,  interval: String| {
            if interval == "Không bao giờ" {
                return Some("".to_string().to_value())
            }
            Some(interval.to_value())
        })
        .flags(BindingFlags::SYNC_CREATE)
        .build();
        row.add_suffix(&edit_button);
        
        row.add_suffix(&delete_button);
        
        
        row.set_activatable_widget(Some(&edit_button));
        row
    }

    fn new_group(&self, str:&String) {
        let group = GroupObject::new(true, str.to_string());
        self.groups().append(&group);
        
    }

    fn new_transaction(&self, transaction_object:TransactionObject) {
        let now = Local::now().format("%d/%m/%Y").to_string();
        let date_now = NaiveDate::parse_from_str(&now, "%d/%m/%Y").unwrap();
        if transaction_object.is_income() {
            self.collection().set_total(self.collection().total() + transaction_object.amount());
            self.collection().set_income(self.collection().income() + transaction_object.amount());
        } else {
            self.collection().set_total(self.collection().total() - transaction_object.amount());
            self.collection().set_expense(self.collection().expense() + transaction_object.amount());
        }
        self.transactions().append(&transaction_object);

        if transaction_object.interval() != "Không bao giờ" {
            let mut end = Local::now().format("%d/%m/%Y").to_string();
            if transaction_object.enddate() != "" {
                end = transaction_object.enddate();
            }
            let date_end = NaiveDate::parse_from_str(&end, "%d/%m/%Y").unwrap();
            if date_now < date_end {
                self.setup_interval(&transaction_object, &date_now);
                return;
            }
            self.setup_interval(&transaction_object, &date_end);
        }
    }

    pub fn setup_callbacks(&self) {
        self.imp().income_check.connect_toggled(clone!(@weak self as wallet => move |_| {
            wallet.set_filter();
        }));
        self.imp().expense_check.connect_toggled(clone!(@weak self as wallet => move |_| {
            wallet.set_filter();
        }));
        self.imp().add_trans.connect_clicked(clone!(@weak self as wallet => move |_| {
            let trans = TransactionDialog::new(&false, &-1);
            let cancel_response = "cancel";
            let create_response = "create";
            
            let dialog = MessageDialog::builder()
            .heading("Tạo Giao Dịch Mới")
            .modal(true)
            .transient_for(&wallet.root()
            .unwrap()
            .downcast::<Window>()
            .unwrap())
            .destroy_with_parent(true)
            .close_response(cancel_response)
            .default_response(create_response)
            .extra_child(&trans)
            .build();
            trans.setup_group(&wallet.groups());
            dialog.add_responses(&[(cancel_response, "Hủy"), (create_response, "Tạo")]);
            dialog.set_response_enabled(create_response, false);

            
            dialog.set_response_appearance(create_response, ResponseAppearance::Suggested);
            dialog.connect_response(
                None,
                clone!(@weak wallet as wallet, @weak trans => move |dialog, response| {
                    // Destroy dialog
                    dialog.destroy();
                    
                    // Return if the user chose a response different than `create_response`
                    if response != create_response {
                        return;
                    }
                    let t = trans.return_object();
                    let mut id = wallet.transactions().n_items();
                    if id != 0 {
                        let o = wallet.transactions().item(id-1).and_downcast::<TransactionObject>().expect("TransactionObject");
                        id = o.id() + 1;
                    }
                    t.set_id(id);
                    wallet.new_transaction(t);
                    wallet.set_filter();
                }),
            );
            dialog.present();
            trans.connect_closure(
                "addvailable", false,
                closure_local!(move |_trans: TransactionDialog, add: bool| {
                    dialog.set_response_enabled(create_response, add);
                }));
        }));

        self.imp()
            .date_button
            .connect_clicked(clone!(@weak self as wallet => move |_| {
                let local_date = Local::now();
                let pop = Popover::builder().build();
                let calendar = Calendar::builder().build();
                if wallet.imp().date.subtitle().unwrap() != "" {
                    let date = chrono::NaiveDate::parse_from_str(&wallet.imp().date.subtitle().unwrap(), "%d/%m/%Y").unwrap();
                    let a = &DateTime::from_local(date.year(), date.month().try_into().unwrap(), date.day().try_into().unwrap(), 0, 0, 0.0).expect("DateTime");
                    calendar.select_day(a);
                }else {
                    wallet.imp().date.set_subtitle(&local_date.format("%d/%m/%Y").to_string());
                    wallet.set_filter();
                }
                pop.set_child(Some(&calendar));
                pop.set_position(PositionType::Bottom);
                pop.set_pointing_to(Some(&wallet.imp().date.allocation()));
                pop.set_parent(&wallet.imp().date.child().unwrap());
                pop.popup();
                calendar.connect_day_selected(clone! (@weak wallet, @weak calendar, @weak pop => move|_| {
                    let date = calendar.date();
                    let text = format!("{}/{}/{}", date.day_of_month(), date.month(), date.year());
                    wallet.imp().date.set_subtitle(&text);
                    pop.hide();
                    wallet.set_filter();
                    // if wallet.imp().date2.subtitle().unwrap().to_string() != "" {
                    //     if wallet.imp().date_type.selected() == 4 
                    //     
                    // }
                    // if (wallet.imp().date_type.selected() == 4) && (wallet.imp().date2.subtitle().unwrap().to_string() != ""){
                    //     let d = &wallet.imp().date.subtitle().unwrap().to_string();
                    //     let d2 = &wallet.imp().date2.subtitle().unwrap().to_string();
                        
                    //     let date1 = NaiveDate::parse_from_str(d, "%d/%m/%Y").unwrap();
                    //     let date2 = NaiveDate::parse_from_str(d2, "%d/%m/%Y").unwrap();
                    //     if date1 <= date2 {
                           
                    //     } else {
                    //         wallet.imp().date.get().add_css_class("error");
                    //         wallet.imp().date2.get().add_css_class("error");
                    //     }
                    // }
                }));
                
                
            })
        );
        self.imp()
            .date_button2
            .connect_clicked(clone!(@weak self as wallet => move |_| {
                let local_date = Local::now();
                let pop = Popover::builder().build();
                let calendar = Calendar::builder().build();
                if wallet.imp().date2.subtitle().unwrap() != "" {
                    let date = chrono::NaiveDate::parse_from_str(&wallet.imp().date2.subtitle().unwrap(), "%d/%m/%Y").unwrap();
                    let a = &DateTime::from_local(date.year(), date.month().try_into().unwrap(), date.day().try_into().unwrap(), 0, 0, 0.0).expect("DateTime");
                    calendar.select_day(a);
                } else {
                    wallet.imp().date2.set_subtitle(&local_date.format("%d/%m/%Y").to_string());
                    wallet.set_filter();
                }
                pop.set_child(Some(&calendar));
                pop.set_position(PositionType::Bottom);
                pop.set_pointing_to(Some(&wallet.imp().date2.allocation()));
                pop.set_parent(&wallet.imp().date2.child().unwrap());
                pop.popup();
                calendar.connect_day_selected(clone! (@weak wallet, @weak calendar, @weak pop => move|_| {
                    let date = calendar.date();
                    let text = format!("{}/{}/{}", date.day_of_month(), date.month(), date.year());
                    wallet.imp().date2.set_subtitle(&text);
                    pop.hide();
                    wallet.set_filter();
                    // if (wallet.imp().date_type.selected() == 4) && (wallet.imp().date.subtitle().unwrap().to_string() != ""){
                    //     let d = &wallet.imp().date.subtitle().unwrap().to_string();
                    //     let d2 = &wallet.imp().date2.subtitle().unwrap().to_string();
                  
                    //     let date1 = NaiveDate::parse_from_str(d, "%d/%m/%Y").unwrap();
                    //     let date2 = NaiveDate::parse_from_str(d2, "%d/%m/%Y").unwrap();
                    //     if date1 <= date2 {
                    //         wallet.set_filter();
                    //     } else {
                    //         wallet.imp().date.get().add_css_class("error");
                    //         wallet.imp().date2.get().add_css_class("error");
                    //     }
                    // }
                }));
            })
        );

        self.imp()
            .add_group
            .connect_clicked(clone!(@weak self as wallet => move |_| {
                let entry_group_name = EntryRow::builder().title("Tên nhóm mới").activates_default(true).build();
                let cancel_response = "cancel";
                let create_response = "create";
                
                let dialog = MessageDialog::builder()
                .heading("Tạo Nhóm Mới")
                .modal(true)
                .transient_for(&wallet.root()
                .unwrap()
                .downcast::<Window>()
                .unwrap())
                .destroy_with_parent(true)
                .close_response(cancel_response)
                .default_response(create_response)
                .extra_child(&entry_group_name)
                .build();
                dialog.add_responses(&[(cancel_response, "Cancel"), (create_response, "Create")]);
                dialog.set_response_enabled(create_response, false);
                entry_group_name.connect_changed(clone!(@weak dialog, @weak wallet => move |entry| {
                    let text = entry.text();
                    let empty = text.is_empty();
                    let dup = wallet.check_dup_group(text.to_string());
                    dialog.set_response_enabled(create_response, (!empty) && (!dup));
        
                }));
                dialog.set_response_appearance(create_response, ResponseAppearance::Suggested);
                dialog.connect_response(
                    None,
                    clone!(@weak wallet as wallet, @weak entry_group_name  => move |dialog, response| {
                        // Destroy dialog
                        dialog.destroy();
        
                        // Return if the user chose a response different than `create_response`
                        if response != create_response {
                            return;
                        }
                        let name = entry_group_name.text().to_string();
                        wallet.new_group(&name);
                    }),
                );
                dialog.present();
            }));

    }

    fn filter(&self) -> Option<CustomFilter> {
        let groups = self.imp().checked.borrow().clone();
        let income = self.income();
        let expense = self.expense();
        let t = self.imp().date_type.selected();
        let date1 = self.imp().date.subtitle().unwrap().to_string();
        let date2 = self.imp().date2.subtitle().unwrap().to_string();
        
        // Create custom filters
        let filter = CustomFilter::new(move |obj| {
            let transaction_object = obj
                .downcast_ref::<TransactionObject>()
                .expect("The object needs to be of type `TransactionObject`.");
                
                let b = if (transaction_object.is_income() && income) || (!transaction_object.is_income() && expense) {
                    true
                } else {
                    false
                };

                let c = if date1 != "" {
                    match t {
                        1 => {transaction_object.is_at(&date1)},
                        2 => {transaction_object.is_before(&date1)},
                        3 => {transaction_object.is_after(&date1)},
                        4 => {if date2 != "" {
                            transaction_object.is_in_range(&date1, &date2)
                        } else {false}},
                        0 | _ => true,
                    }
                } else {
                    if t == 0 {
                        true
                    } else {
                        false
                    }
                };
                

                let a = groups.contains(&transaction_object.group()); 
                a&&b&&c
        });
        

        // Return the correct filter
        Some(filter)
    }

    fn check_dup_group(&self, str:String) -> bool{
        let mut position = 0;
        while let Some(item) = self.groups().item(position) {
            // Get `TaskObject` from `glib::Object`
            let group_object = item
                .downcast_ref::<GroupObject>()
                .expect("The object needs to be of type `GroupObject`.");

            if group_object.title() == str {
                return true;
            } else {
                position += 1;
            }
        }
        return false;
    }

    fn delete_transaction(&self, id:u32) {
        let transactions = self.transactions();
        let mut position = 0;

        while let Some(item) = self.transactions().item(position) {
            // Get `TaskObject` from `glib::Object`
            let transaction_object = item
                .downcast_ref::<TransactionObject>()
                .expect("The object needs to be of type `TransactionObject`.");

            if transaction_object.id() == id {
                if transaction_object.is_income() {
                    self.collection().set_total(self.collection().total() - transaction_object.amount());
                    self.collection().set_income(self.collection().income() - transaction_object.amount());
                } else {
                    self.collection().set_total(self.collection().total() + transaction_object.amount());
                    self.collection().set_expense(self.collection().expense() - transaction_object.amount());
                }
                transactions.remove(position);
                return;
            } else {
                position += 1;
            }
        }
    }

    fn delete_interval(&self, source:i32) {
        let transactions = self.transactions();
        let mut position = 0;

        while let Some(item) = self.transactions().item(position) {
            // Get `TaskObject` from `glib::Object`
            let transaction_object = item
                .downcast_ref::<TransactionObject>()
                .expect("The object needs to be of type `TransactionObject`.");

            if transaction_object.source() == source {
                if transaction_object.is_income() {
                    self.collection().set_total(self.collection().total() - transaction_object.amount());
                    self.collection().set_income(self.collection().income() - transaction_object.amount());
                } else {
                    self.collection().set_total(self.collection().total() + transaction_object.amount());
                    self.collection().set_expense(self.collection().expense() - transaction_object.amount());
                }
                transactions.remove(position);
            } else {
                position += 1;
            }
        }
    }

    fn delete_group(&self, title:String, delete:bool) {
        let transactions = self.transactions();
        let mut position = 0;

        while let Some(item) = self.transactions().item(position) {
            // Get `TaskObject` from `glib::Object`
            let transaction_object = item
                .downcast_ref::<TransactionObject>()
                .expect("The object needs to be of type `TransactionObject`.");

            if transaction_object.group() == title {
                if delete {
                    if transaction_object.is_income() {
                        self.collection().set_total(self.collection().total() - transaction_object.amount());
                        self.collection().set_income(self.collection().income() - transaction_object.amount());
                    } else {
                        self.collection().set_total(self.collection().total() + transaction_object.amount());
                        self.collection().set_expense(self.collection().expense() - transaction_object.amount());
                    }
                    transactions.remove(position);
                } else {
                    transaction_object.set_group("Chưa có nhóm");
                }
            } else {
                position += 1;
            }
        }
        let groups = self.groups();
        let mut position = 0;
        while let Some(item) = self.groups().item(position) {
            // Get `TaskObject` from `glib::Object`
            let group_object = item
                .downcast_ref::<GroupObject>()
                .expect("The object needs to be of type `GroupObject`.");

            if group_object.title() == title {
                groups.remove(position);
                return;
            } else {
                position += 1;
            }
        }
        
    }

    fn find_last_added_day(&self, source:i32) -> NaiveDate {
        let mut position = self.transactions().n_items()-1;
        let mut date_now= Local::now().date_naive();
        while let Some(item) = self.transactions().item(position) {
        
            // Get `TaskObject` from `glib::Object`
            let transaction_object = item
                .downcast_ref::<TransactionObject>()
                .expect("The object needs to be of type `TransactionObject`.");
            if transaction_object.id() as i32 == source {
                date_now = NaiveDate::parse_from_str(&transaction_object.date(), "%d/%m/%Y").unwrap();
                
            }
            if transaction_object.source() == source {
                if NaiveDate::parse_from_str(&transaction_object.date(), "%d/%m/%Y").unwrap() >= date_now {
                    date_now = NaiveDate::parse_from_str(&transaction_object.date(), "%d/%m/%Y").unwrap();
                    return date_now;
                }
            }
            if position != 0 {
                position -= 1;
            } else {
                break;
            }
            
            
        }
        date_now
    }
    
    fn setup_interval(&self, transaction_object:&TransactionObject, date_end: &NaiveDate) {
        let o = self.transactions().item(self.transactions().n_items()-1).and_downcast::<TransactionObject>().expect("TransactionObject");
        let mut id = o.id() + 1;
        let mut date_start = NaiveDate::parse_from_str(&transaction_object.date(), "%d/%m/%Y").unwrap();
        let income = transaction_object.is_income();
        while date_start <= *date_end {
            match transaction_object.interval().as_str() {
                "Hằng ngày" => {
                    date_start = date_start.checked_add_days(Days::new(1)).unwrap();
                },
                "Hằng tuần" => {
                    date_start = date_start.checked_add_days(Days::new(7)).unwrap();
                },
                "Hằng tháng" => {
                    date_start = date_start.checked_add_months(Months::new(1)).unwrap();
                },
                "Hằng năm" => {
                    date_start = date_start.checked_add_months(Months::new(12)).unwrap();
                },
                _ => self.delete_interval(transaction_object.id().try_into().unwrap()),
            };
            if date_start > *date_end {
                break;
            }
            if income {
                self.collection().set_total(self.collection().total() + transaction_object.amount());
                self.collection().set_income(self.collection().income() + transaction_object.amount());
            } else {
                self.collection().set_total(self.collection().total() - transaction_object.amount());
                self.collection().set_expense(self.collection().expense() + transaction_object.amount());
            }
            self.transactions().append(&TransactionObject::new(id, transaction_object.id().try_into().unwrap(), transaction_object.description(), transaction_object.amount(), transaction_object.income(), transaction_object.group(), date_start.format("%d/%m/%Y").to_string(), "Không bao giờ".to_string(), transaction_object.enddate(), true));
            id += 1; 
        }
    }

    fn search_for_needed_update(&self) {
        let mut position = 0;

        while let Some(item) = self.transactions().item(position) {
            // Get `TaskObject` from `glib::Object`
            let transaction_object = item
                .downcast_ref::<TransactionObject>()
                .expect("The object needs to be of type `TransactionObject`.");

            if !transaction_object.done() {
                self.update(transaction_object);
                
            } 
             position += 1;
        }
    }

    fn update(&self, transaction_object:&TransactionObject) {
        if transaction_object.done() {
            return;
        }
        let mut last_date = self.find_last_added_day(transaction_object.id().try_into().unwrap());
        let enddate = if transaction_object.enddate() != "" {
            NaiveDate::parse_from_str(&transaction_object.enddate(), "%d/%m/%Y").unwrap()
        } else {
            NaiveDate::MAX
        };
        let local = Local::now().date_naive();
        let o = self.transactions().item(self.transactions().n_items()-1).and_downcast::<TransactionObject>().expect("TransactionObject");
        let mut id = o.id() + 1;
        let income = transaction_object.is_income();
        if enddate > local {
            while last_date <= local {
                match transaction_object.interval().as_str() {
                    "Hằng ngày" => {
                        last_date = last_date.checked_add_days(Days::new(1)).unwrap();
                    },
                    "Hằng tuần" => {
                        last_date = last_date.checked_add_days(Days::new(7)).unwrap();
                    },
                    "Hằng tháng" => {
                        last_date = last_date.checked_add_months(Months::new(1)).unwrap();
                    },
                    "Hằng năm" => {
                        last_date = last_date.checked_add_months(Months::new(12)).unwrap();
                    },
                    _ => self.delete_interval(transaction_object.id().try_into().unwrap()),
                };
                if last_date > local {
                    break;
                }
                if income {
                    self.collection().set_total(self.collection().total() + transaction_object.amount());
                    self.collection().set_income(self.collection().income() + transaction_object.amount());
                } else {
                    self.collection().set_total(self.collection().total() - transaction_object.amount());
                    self.collection().set_expense(self.collection().expense() + transaction_object.amount());
                }
                self.transactions().append(&TransactionObject::new(id, transaction_object.id().try_into().unwrap(), transaction_object.description(), transaction_object.amount(), transaction_object.income(), transaction_object.group(), last_date.format("%d/%m/%Y").to_string(), "Không bao giờ".to_string(), transaction_object.enddate(), true));
                id += 1; 
            }
            return;
        } else {
            
            while last_date <= enddate {
                
                match transaction_object.interval().as_str() {
                    "Hằng ngày" => {
                        last_date = last_date.checked_add_days(Days::new(1)).unwrap();
                    },
                    "Hằng tuần" => {
                        last_date = last_date.checked_add_days(Days::new(7)).unwrap();
                    },
                    "Hằng tháng" => {
                        last_date = last_date.checked_add_months(Months::new(1)).unwrap();
                    },
                    "Hằng năm" => {
                        last_date = last_date.checked_add_months(Months::new(12)).unwrap();
                    },
                    _ => self.delete_interval(transaction_object.id().try_into().unwrap()),
                };
                if last_date > enddate {
                    break;
                }
                if income {
                    self.collection().set_total(self.collection().total() + transaction_object.amount());
                    self.collection().set_income(self.collection().income() + transaction_object.amount());
                } else {
                    self.collection().set_total(self.collection().total() - transaction_object.amount());
                    self.collection().set_expense(self.collection().expense() + transaction_object.amount());
                }
                self.transactions().append(&TransactionObject::new(id, transaction_object.id().try_into().unwrap(), transaction_object.description(), transaction_object.amount(), transaction_object.income(), transaction_object.group(), last_date.format("%d/%m/%Y").to_string(), "Không bao giờ".to_string(), transaction_object.enddate(), true));
                id += 1; 
            }
            transaction_object.set_done(true);
            return;
        }
    }

}
