use adw::{ActionRow, PreferencesGroup, ComboRow};
use gtk::glib::subclass::Signal;
use gtk::glib::{self, once_cell, ParamSpec, Value, SignalHandlerId, Properties};

use glib::subclass::InitializingObject;
use adw::{prelude::*, subclass::prelude::*};
use gtk::{Button, FilterListModel, FlowBox, ListBox, ScrolledWindow, CheckButton};
use gtk::{CompositeTemplate};
use once_cell::sync::{Lazy, OnceCell};

use std::cell::RefCell;
use std::fs::File;


use crate::account_collection::{CollectionObject, CollectionData};

use crate::utils::data_path;


// ANCHOR: object
// Object holding the state
#[derive(Default, CompositeTemplate, Properties)]
#[template(resource = "/org/gtk_rs/example/wallet_tab.ui")]
#[properties(wrapper_type = super::Wallet)]
pub struct Wallet {
    #[property(get, set)]
    location: RefCell<String>,

    #[template_child]
    pub add_trans: TemplateChild<Button>,

    #[template_child]
    pub transaction_list: TemplateChild<FlowBox>,
    #[template_child]
    pub group_list: TemplateChild<ListBox>,
    #[template_child]
    pub group_scroll: TemplateChild<ScrolledWindow>,
    #[template_child]
    pub overview: TemplateChild<PreferencesGroup>,
    #[template_child]
    pub total_row: TemplateChild<ActionRow>,
    #[template_child]
    pub expense_row: TemplateChild<ActionRow>,
    #[template_child]
    pub income_row: TemplateChild<ActionRow>,
    #[template_child]
    pub add_group: TemplateChild<Button>,
    #[template_child]
    pub date: TemplateChild<ActionRow>,
    #[template_child]
    pub date_button: TemplateChild<Button>,
    #[template_child]
    pub date2: TemplateChild<ActionRow>,
    #[template_child]
    pub date_button2: TemplateChild<Button>,
    #[template_child]
    pub date_type: TemplateChild<ComboRow>,
    #[template_child]
    pub income_check: TemplateChild<CheckButton>,
    #[template_child]
    pub expense_check: TemplateChild<CheckButton>,


    pub current_filter_model: RefCell<Option<FilterListModel>>,
    pub transactions_changed_handler_id: RefCell<Option<SignalHandlerId>>,
    #[property(get, set)]
    pub income:RefCell<bool>,
    #[property(get, set)]
    pub expense:RefCell<bool>,
    pub checked: RefCell<Vec<String>>,
    pub collection: OnceCell<CollectionObject>,
    
    
}
// ANCHOR_END: object

// ANCHOR: subclass
// The central trait for subclassing a GObject
#[glib::object_subclass]
impl ObjectSubclass for Wallet {
    // `NAME` needs to match `class` attribute of template
    const NAME: &'static str = "WalletTab";
    type Type = super::Wallet;
    type ParentType = gtk::Box;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}



// Trait shared by all GObjects
impl ObjectImpl for Wallet {
    fn properties() -> &'static [ParamSpec] {
        Self::derived_properties()
    }

    fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
        self.derived_set_property(id, value, pspec)
    }

    fn property(&self, id: usize, pspec: &ParamSpec) -> Value {
        self.derived_property(id, pspec)
    }
    
    fn constructed(&self) {
        // Call "constructed" on parent
        self.parent_constructed();
        
        // Setup
        
    
        // obj.restore_data();
        // obj.setup_overview();
        // obj.setup_callbacks();
        // obj.setup_actions();
        
    }
    fn signals() -> &'static [Signal] {
        static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
            vec![Signal::builder("closed")
                .build()]
        });
        SIGNALS.as_ref()
    }
    fn dispose(&self) {
        
        let backup_data: CollectionData = self.obj().collection().to_collection_data();
        self.obj().emit_by_name::<()>("closed", &[]);
        let file = File::create(data_path(&backup_data.name)).expect("Could not create json file.");
        serde_json::to_writer(file, &backup_data)
            .expect("Could not write data to json file");
    }
}

// Trait shared by all widgets
impl WidgetImpl for Wallet {}

// Trait shared by all boxes
impl BoxImpl for Wallet {
}



