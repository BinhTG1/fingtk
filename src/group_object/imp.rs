use std::cell::RefCell;

use glib::{ParamSpec, Properties, Value};
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

use super::GroupData;

// Object holding the state
#[derive(Properties, Default)]
#[properties(wrapper_type = super::GroupObject)]
pub struct GroupObject {
    #[property(name = "checked", get, set, type = bool, member = checked)]
    #[property(name = "title", get, set, type = String, member = title)]
    pub data: RefCell<GroupData>,
}

// The central trait for subclassing a GObject
#[glib::object_subclass]
impl ObjectSubclass for GroupObject {
    const NAME: &'static str = "GroupObject";
    type Type = super::GroupObject;
}

// Trait shared by all GObjects
impl ObjectImpl for GroupObject {
    fn properties() -> &'static [ParamSpec] {
        Self::derived_properties()
    }

    fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
        self.derived_set_property(id, value, pspec)
    }

    fn property(&self, id: usize, pspec: &ParamSpec) -> Value {
        self.derived_property(id, pspec)
    }
}