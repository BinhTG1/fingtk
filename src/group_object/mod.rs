mod imp;

use adw::subclass::prelude::*;
use glib::Object;
use gtk::glib;
use serde::{Deserialize, Serialize};

glib::wrapper! {
    pub struct GroupObject(ObjectSubclass<imp::GroupObject>);
}

impl GroupObject {
    pub fn new(checked: bool, title: String) -> Self {
        Object::builder()
            .property("checked", checked)
            .property("title", title)
            .build()
    }

    pub fn is_checked(&self) -> bool {
        self.imp().data.borrow().checked
    }

    pub fn group_data(&self) -> GroupData {
        self.imp().data.borrow().clone()
    }

    pub fn from_group_data(group_data: GroupData) -> Self {
        Self::new(group_data.checked, group_data.title)
    }
}

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct GroupData {
    pub checked: bool,
    pub title: String,
}