
mod imp;

use std::fs::{File, self};
use crate::APP_ID;
use crate::account_row::AccountObject;
use crate::utils::data_path;

use chrono::{NaiveTime, Local};
use glib::{clone, Object};

use adw::subclass::prelude::*;
use adw::{prelude::*, StatusPage, EntryRow, MessageDialog, ResponseAppearance, ActionRow, ButtonContent, PreferencesGroup, ComboRow, StyleManager};

use gtk::gio::{Settings, SettingsBindFlags};
use gtk::glib::closure_local;
use gtk::{gio, glib, Button, Box, ListBox, NoSelection, ScrolledWindow, Switch, Inhibit, StringList};
use crate::account_collection::{CollectionObject, CollectionData};
use crate::group_object::GroupObject;
use gio::Icon;
use crate::transaction_object::TransactionObject;

use rand::seq::SliceRandom;

use crate::wallet_tab::Wallet;

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends adw::ApplicationWindow, gtk::ApplicationWindow, gtk::Window, gtk::Widget,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Accessible, gtk::Buildable,
                    gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl Window {
    pub fn new(app: &adw::Application) -> Self {
        // Create new window
        Object::builder().property("application", app).build()
    }

    fn setup_actions(&self) {
        let action_new_account = gio::SimpleAction::new("new-account", None);
        action_new_account.connect_activate(clone!(@weak self as window => move |_, _| {
            window.new_account();
        }));
        self.add_action(&action_new_account);
    }

    fn get_icon(&self) -> String {
        let low = NaiveTime::from_hms_opt(6, 0, 0).unwrap(); // 6 am
        let high = NaiveTime::from_hms_opt(18, 0, 0).unwrap(); // 6 pm
        let current_time = Local::now().time();
        let day_list = vec!["weather-clear-symbolic", "weather-few-clouds-symbolic"];
        let night_list = vec!["weather-clear-night-symbolic", "weather-few-clouds-night-symbolic"];
        if (current_time > low) && (current_time < high) {
            day_list.choose(&mut rand::thread_rng()).unwrap().to_string()
        } else {
            night_list.choose(&mut rand::thread_rng()).unwrap().to_string()
        }
    }

    fn get_time(&self) -> String {
        let low = NaiveTime::from_hms_opt(6, 0, 0).unwrap(); // 6 am
        let high = NaiveTime::from_hms_opt(18, 0, 0).unwrap(); // 6 pm
        let current_time = Local::now().time();
        if (current_time > low) && (current_time < high) {
            "Chào buổi sáng".to_string()
        } else {
            "Chào buổi tối".to_string()
        }
    }

    pub fn main_tab(&self) {
        let layout = Box::builder().orientation(gtk::Orientation::Vertical).spacing(10).halign(gtk::Align::Center).build();
        let button = Button::builder().label("Tạo tài khoản mới").halign(gtk::Align::Center).height_request(20).width_request(20).build();
        layout.append(&button);
        let scroll = ScrolledWindow::builder().halign(gtk::Align::Center).height_request(200).width_request(500).build();
        layout.append(&scroll);
        let list_box = ListBox::builder().build();
        scroll.set_child(Some(&list_box));
        button.connect_clicked(move |button| {
            button
                .activate_action("win.new-account", None)
                .expect("The action does not exist.");
        });
        self.setup_accounts();
        let accounts = self.accounts();
        let selection_model = NoSelection::new(Some(accounts.clone()));
        list_box.bind_model( Some(&selection_model),
            clone!(@weak self as window => @default-panic, move |obj| {
                let account = obj
                    .downcast_ref()
                    .expect("The object should be of type `AccountObject`.");
                let row = window.create_account_row(account);
                row.upcast()
            }),
        );
        let icon_name = self.get_icon();
        let welcome = self.get_time();
        let status_page = StatusPage::builder().icon_name(icon_name).title(welcome).description("Mở tài khoản hoặc tạo tài khoản mới").build();
        status_page.set_child(Some(&layout));
    
        
        let page = self.imp().view.insert_pinned(&status_page, 0);
        page.set_icon(Some(&Icon::for_string("user-home-symbolic").expect("icon")));
    }

    pub fn setup_settings(&self) {
        let settings = Settings::new(APP_ID);
        self.imp()
            .settings
            .set(settings)
            .expect("`settings` should not be set before calling `setup_settings`.");
    }

    fn settings(&self) -> &Settings {
        self.imp()
            .settings
            .get()
            .expect("`settings` should be set in `setup_settings`.")
    }

    pub fn settings_tab(&self) {
        self.setup_settings();
        let preferences_group = PreferencesGroup::builder().margin_top(20).margin_start(20).margin_end(20).build();
        preferences_group.set_title("Cài đặt ứng dụng");
        let size_row = ComboRow::builder().title("Độ lớn cửa sổ").build();
        let width = self.settings().int("width");
        let height = self.settings().int("height");
        let is_maximized = self.settings().boolean("is-maximized");

        // Set the size of the window
        self.set_default_size(width, height);

        // If the window was maximized when it was closed, maximize it again
        if is_maximized {
            self.maximize();
        }
        let window_sizes: Vec<(i32, i32)> = vec![
            (640, 480),
            (800, 600),
            (1024, 768),
            (1280, 720),
            (1366, 768),
        ];
        let model:StringList = StringList::new(&[]);
        model.append("Tùy chọn");

        for (width, height) in &window_sizes {
            model.append(&format!("{}x{}", width, height))
        }

        
        let s = self.default_size();
        if let Some(index) = window_sizes.iter().position(|&size| size == s) {
            size_row.set_selected(index.try_into().unwrap());
        } else {
            size_row.set_selected(0);
        }
        size_row.connect_selected_notify(clone!(@weak self as window => move |row| {
            let pos = row.selected();
            match pos {
                1|2|3|4|5 => {let size = window_sizes[usize::try_from(pos-1).unwrap()];
    
                             window.set_default_size(size.0, size.1);},
                _ => {}
            }
        }));
        self.connect_default_height_notify(clone!(@weak size_row => move |window| {
            let s = window.default_size();
            let sizes: Vec<(i32, i32)> = vec![
            (640, 480),
            (800, 600),
            (1024, 768),
            (1280, 720),
            (1366, 768),
        ];
            if let Some(index) = sizes.iter().position(|&size| size == s) {
                size_row.set_selected((index + 1).try_into().unwrap());
            } else {
                size_row.set_selected(0);
            }
        }));
        self.connect_default_width_notify(clone!(@weak size_row => move |window| {
            let s = window.default_size();
            let sizes: Vec<(i32, i32)> = vec![
            (640, 480),
            (800, 600),
            (1024, 768),
            (1280, 720),
            (1366, 768),
        ];
            if let Some(index) = sizes.iter().position(|&size| size == s) {
                size_row.set_selected((index + 1).try_into().unwrap());
            } else {
                size_row.set_selected(0);
            }
        }));
        size_row.set_model(Some(&model));
        let dark_mode_row = ActionRow::builder().title("Chế độ tối").build();
        let dark_switch = Switch::builder().valign(gtk::Align::Center).halign(gtk::Align::Center).build();
        self.settings().bind("dark-enabled", &dark_switch, "active").flags(SettingsBindFlags::DEFAULT)
        .build();
        let style = StyleManager::default();
        if self.settings().get("dark-enabled") {
            style.set_color_scheme(adw::ColorScheme::PreferDark);
        }
        dark_switch.connect_state_set(move |_switch, state| {
            if state {
                style.set_color_scheme(adw::ColorScheme::PreferDark);
            } else {
                style.set_color_scheme(adw::ColorScheme::PreferLight);
            }
            Inhibit(false)
        });
        dark_mode_row.set_activatable_widget(Some(&dark_switch));
        dark_mode_row.add_suffix(&dark_switch);
        preferences_group.add(&size_row);
        preferences_group.add(&dark_mode_row);
        let page = self.imp().view.insert_pinned(&preferences_group, 1);
        page.set_icon(Some(&Icon::for_string("preferences-system-symbolic").expect("icon")));
    }

    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let size = self.default_size();
        self.settings().set_int("width", size.0)?;
        self.settings().set_int("height", size.1)?;
        self.settings()
            .set_boolean("is-maximized", self.is_maximized())?;

        Ok(())
    }

    pub fn new_account(&self) {
        let entry_name = EntryRow::builder().title("Tên tài khoản").activates_default(true).build();
        let entry_currency = EntryRow::builder().title("Loại tiền tệ").input_purpose(gtk::InputPurpose::Alpha).input_hints(gtk::InputHints::UPPERCASE_CHARS).build();
        let entry_ini = EntryRow::builder().title("Lượng tiền khởi điểm").input_purpose(gtk::InputPurpose::Number).build();
        let b = Box::builder().orientation(gtk::Orientation::Vertical).spacing(6).build();
        b.append(&entry_name);
        b.append(&entry_currency);
        b.append(&entry_ini);

        let cancel_response = "Hủy";
        let create_response = "Tạo";
        let dialog = MessageDialog::builder()
            .heading("Tạo Tài Khoản Mới")
            .transient_for(self)
            .modal(true)
            .destroy_with_parent(true)
            .close_response(cancel_response)
            .default_response(create_response)
            .extra_child(&b)
            .build();
        dialog.add_responses(&[(cancel_response, "Hủy"), (create_response, "Tạo")]);


        // Make the dialog button insensitive initially
        dialog.set_response_enabled(create_response, false);
        dialog.set_response_appearance(create_response, ResponseAppearance::Suggested);

        entry_name.connect_changed(clone!(@weak dialog, @weak entry_currency, @weak entry_ini, @weak self as window => move |entry| {
            let text = entry.text();
            let empty1 = text.is_empty();
            let empty2 = entry_currency.text().is_empty();
            let empty3 = match entry_ini.text().parse::<f64>() {
                Ok(_a) => {
                    true
                }
                Err(_e) => {
                    false
                }
            };
            let b = window.check_dup(text.as_str());
            dialog.set_response_enabled(create_response, (!empty1) && (!empty2) && empty3 && b);
        }));

        entry_currency.connect_changed(clone!(@weak dialog, @weak entry_name, @weak entry_ini, @weak self as window => move |entry| {
            let text = entry.text();
            let empty1 = text.is_empty();
            let empty2 = entry_name.text().is_empty();
            let empty3 = match entry_ini.text().parse::<f64>() {
                Ok(_a) => {
                    true
                }
                Err(_e) => {
                    false
                }
            };
            let b = window.check_dup(entry_name.text().as_str());
            dialog.set_response_enabled(create_response, (!empty1) && (!empty2) && empty3 && b);

        }));

        entry_ini.connect_changed(clone!(@weak dialog, @weak entry_name, @weak entry_currency, @weak self as window => move |entry| {
            let empty1 = entry_currency.text().is_empty();
            let empty2 = entry_name.text().is_empty();
            let empty3 = match entry.text().parse::<f64>() {
                Ok(_a) => {
                    true
                }
                Err(_e) => {
                    false
                }
            };
            let b = window.check_dup(entry_name.text().as_str());
            dialog.set_response_enabled(create_response, (!empty1) && (!empty2) && empty3 && b);

        }));

        dialog.connect_response(
            None,
            clone!(@weak self as window, @weak entry_name, @weak entry_currency, @weak entry_ini  => move |dialog, response| {
                // Destroy dialog
                dialog.destroy();

                // Return if the user chose a response different than `create_response`
                if response != create_response {
                    return;
                }

                let name = entry_name.text().to_string();
                let currency = entry_currency.text().to_string();
                let ini:f64 = match entry_ini.text().to_string().parse() {
                    Ok(num) => num,
                    Err(_) => 0.0,
                };
                let transactions = gio::ListStore::new(TransactionObject::static_type());
                let groups = gio::ListStore::new(GroupObject::static_type());
                groups.append(&GroupObject::new(true, "Chưa có nhóm".to_string()));
                
                let collection = CollectionObject::new(&name, &currency, &ini, &0.0, &0.0, groups, transactions);

                let backup_data: CollectionData = collection.to_collection_data();

                let file = File::create(data_path(&name)).expect("Could not create json file.");
                serde_json::to_writer(file, &backup_data)
                    .expect("Could not write data to json file");
                let account_new = AccountObject::new(&name, &data_path(&name).as_path().display().to_string());
                let accounts = window.accounts();
                accounts.append(&account_new);
            }),
        );
        dialog.present();
    }

    fn setup_accounts(&self) {
        let model = gio::ListStore::new(AccountObject::static_type());
        let mut path = glib::user_data_dir();
        path.push(APP_ID);
        if let Ok(entries) = fs::read_dir(&path) {
            for entry in entries {
                if let Ok(entry) = entry {
                    if let Some(file_name) = entry.file_name().to_str() {
                        let name = &file_name[0..file_name.len() - 5];
                        let mut p = path.as_path().display().to_string();
                        p.push_str(r"\");
                        p.push_str(file_name);
                        model.append(&AccountObject::new(&name, &p));
                    }
                }
            }
        }
        self.imp().accounts.set(model).expect("Could not set accounts");
    }

    pub fn create_account_row(&self, account:&AccountObject) -> ActionRow {
        let row = ActionRow::builder().title(account.name()).subtitle(account.path()).subtitle_lines(1).build();
        let delete_button = Button::builder().child(&ButtonContent::builder().icon_name("edit-delete").use_underline(true).build()).has_frame(false).build();
        let button = Button::builder().child(&ButtonContent::builder().icon_name("folder-symbolic").label("Mở tài Khoản").use_underline(true).build()).has_frame(false).build();
        button.connect_clicked(clone!(@weak self as window,@weak account, @weak button, @weak row, @weak delete_button => move |_| {
            let wallet = Wallet::new();
            window.imp().view.append(&wallet).set_title(&account.name());
            wallet.restore_data(&account.name());
            wallet.setup_overview();
            wallet.setup_group();
            wallet.setup_date_filter();
            wallet.setup_callbacks();
            wallet.setup_transaction();
            button.set_sensitive(false);
            delete_button.set_sensitive(false);
            wallet.connect_closure(
                "closed", false,
                closure_local!(move |_wallet: Wallet| {
                    button.set_sensitive(true);
                    delete_button.set_sensitive(true);
                    window.imp().view.set_selected_page(&window.imp().view.nth_page(0));
                }));
        }));
        
        delete_button.connect_clicked(clone!(@weak self as window,@weak account, @weak button, @weak row => move |_| {
            let cancel_response = "cancel";
                let delete_response = "delete";
                
                let dialog = MessageDialog::builder()
                .heading("Xoá tài khoản")
                .body(format!("Lưu ý việc xóa tài khoản {} không thể hoàn tác", account.name()))
                .modal(true)
                .transient_for(&window)
                .destroy_with_parent(true)
                .close_response(cancel_response)
                .default_response(delete_response)
                .build();
                
                dialog.add_responses(&[(cancel_response, "Hủy"), (delete_response, "Xóa")]);
              
                dialog.set_response_appearance(delete_response, ResponseAppearance::Destructive);
                dialog.connect_response(
                    None,
                    clone!(@weak window, @weak account => move |dialog, response| {
                        // Destroy dialog
                        dialog.destroy();
                        
                        // Return if the user chose a response different than `create_response`
                        if response == cancel_response {
                            return;
                        }
                        match fs::remove_file(account.path()) {
                            Ok(()) => {let accounts = window.accounts();
                                        let mut position = 0;
                
                                        while let Some(item) = window.accounts().item(position) {
                                            // Get `TaskObject` from `glib::Object`
                                            let account_object = item
                                                .downcast_ref::<AccountObject>()
                                                .expect("The object needs to be of type `AccountObject`.");
                
                                            if account_object.path() == account.path() {
                                                accounts.remove(position);
                                                return;
                                            } else {
                                                position += 1;
                                            }
                                        }   } ,
                            Err(e) => {
                                eprintln!("Error removing file: {}", e);
                                std::process::exit(1);
                            }
                        }
                        
                        
                    }),
                );
                dialog.present();
        }));
        
        row.add_suffix(&button);
        row.add_suffix(&delete_button);
        row
    }

    fn accounts(&self) -> gio::ListStore {
        self.imp().accounts.get()
        .expect("`accounts` should be set in `setup_accounts")
        .clone()
    }
    fn check_dup(&self, name:&str) -> bool {
        let mut position = 0;

        while let Some(item) = self.accounts().item(position) {
            // Get `TaskObject` from `glib::Object`
            let account_object = item
                .downcast_ref::<AccountObject>()
                .expect("The object needs to be of type `account_object`.");

            if account_object.name() == name {
                return false;
            } 
            position += 1;
        }
        true
    }
}
