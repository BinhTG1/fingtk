use gtk::glib::{self,once_cell};

use glib::subclass::InitializingObject;
use adw::{subclass::prelude::*, TabBar, TabView};

use gtk::{CompositeTemplate, Inhibit};

use gtk::gio::{self, Settings};
use once_cell::sync::OnceCell;



// ANCHOR: object
// Object holding the state
#[derive(Default, CompositeTemplate)]
#[template(resource = "/org/gtk_rs/example/window.ui")]
pub struct Window {
    // #[template_child]
    // pub notebook: TemplateChild<Notebook>,
    #[template_child]
    pub tab_bar: TemplateChild<TabBar>,
    #[template_child]
    pub view: TemplateChild<TabView>,
    pub settings: OnceCell<Settings>,
    pub accounts: OnceCell<gio::ListStore>,
}
// ANCHOR_END: object

// ANCHOR: subclass
// The central trait for subclassing a GObject
#[glib::object_subclass]
impl ObjectSubclass for Window {
    // `NAME` needs to match `class` attribute of template
    const NAME: &'static str = "MyGtkAppWindow";
    type Type = super::Window;
    type ParentType = adw::ApplicationWindow;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();

    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

// Trait shared by all GObjects
impl ObjectImpl for Window {
    fn constructed(&self) {
        // Call "constructed" on parent
        self.parent_constructed();
        
        // Setup
        let obj = self.obj();
        obj.main_tab();
        obj.settings_tab();
        obj.setup_actions();
    }
    
}

// Trait shared by all widgets
impl WidgetImpl for Window {}

// Trait shared by all windows
impl WindowImpl for Window {
    fn close_request(&self) -> Inhibit {
        // Save window size
        self.obj()
            .save_window_size()
            .expect("Failed to save window state");

        // Don't inhibit the default handler
        Inhibit(false)
    }
}

impl ApplicationWindowImpl for Window {}

// ANCHOR: adw_application_window_impl
// Trait shared by all adwaita application windows
impl AdwApplicationWindowImpl for Window {}

