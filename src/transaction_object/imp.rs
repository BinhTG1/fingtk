use std::cell::RefCell;

use glib::{ParamSpec, Properties, Value};
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

use super::TransactionData;

// Object holding the state
#[derive(Properties, Default)]
#[properties(wrapper_type = super::TransactionObject)]
pub struct TransactionObject {
    #[property(name = "id", get, set, type = u32, member = id)]
    #[property(name = "source", get, set, type = i32, member = source)]
    #[property(name = "description", get, set, type = String, member = description)]
    #[property(name = "amount", get, set, type = f64, member = amount)]
    #[property(name = "income", get, set, type = bool, member = income)]
    #[property(name = "group", get, set, type = String, member = group)]
    #[property(name = "date", get, set, type = String, member = date)]
    #[property(name = "interval", get, set, type = String, member = interval)]
    #[property(name = "enddate", get, set, type = String, member = enddate)]
    #[property(name = "done", get, set, type = bool, member = done)]
    pub data: RefCell<TransactionData>,

}

// The central trait for subclassing a GObject
#[glib::object_subclass]
impl ObjectSubclass for TransactionObject {
    const NAME: &'static str = "TransactionObject";
    type Type = super::TransactionObject;
}

// Trait shared by all GObjects
impl ObjectImpl for TransactionObject {
    fn properties() -> &'static [ParamSpec] {
        Self::derived_properties()
    }

    fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
        self.derived_set_property(id, value, pspec)
    }

    fn property(&self, id: usize, pspec: &ParamSpec) -> Value {
        self.derived_property(id, pspec)
    }
}