mod imp;
use chrono::prelude::*;
use adw::subclass::prelude::*;
use glib::Object;
use gtk::glib;
use serde::{Deserialize, Serialize};

glib::wrapper! {
    pub struct TransactionObject(ObjectSubclass<imp::TransactionObject>);
}

impl TransactionObject {
    pub fn new(id: u32, source: i32, description: String, amount: f64, income: bool, group: String, date: String, interval: String, enddate: String, done:bool) -> Self {
        Object::builder()
            .property("id", id)
            .property("source", source)
            .property("description", description)
            .property("amount", amount)
            .property("income", income)
            .property("group", group)
            .property("date", date)
            .property("interval", interval)
            .property("enddate", enddate)
            .property("done", done)
            .build()
    }

    pub fn is_income(&self) -> bool {
        self.imp().data.borrow().income
    }

    pub fn transaction_data(&self) -> TransactionData {
        self.imp().data.borrow().clone()
    }

    fn transaction_date(&self) -> String{
        self.imp().data.borrow().date.clone()
    }
    fn transaction_amount(&self) -> f64{
        self.imp().data.borrow().amount
    }
    fn transaction_interval(&self) -> String{
        self.imp().data.borrow().interval.clone()
    }

    pub fn is_in_range(&self, from:&String, to:&String) -> bool {
        let parsed_from = NaiveDate::parse_from_str(from, "%d/%m/%Y").unwrap();
        let parsed_to = NaiveDate::parse_from_str(to, "%d/%m/%Y").unwrap();
        let parsed_transaction_date = NaiveDate::parse_from_str(&self.transaction_date(), "%d/%m/%Y").unwrap();
        (parsed_transaction_date <= parsed_to) && (parsed_transaction_date >= parsed_from)
    }

    pub fn is_at(&self, date:&String) -> bool {
        let parsed_date = NaiveDate::parse_from_str(date, "%d/%m/%Y").unwrap();
        let parsed_transaction_date = NaiveDate::parse_from_str(&self.transaction_date(), "%d/%m/%Y").unwrap();
        parsed_date == parsed_transaction_date
    }

    pub fn is_before(&self, date:&String) -> bool {
        let parsed_date = NaiveDate::parse_from_str(date, "%d/%m/%Y").unwrap();
        let parsed_transaction_date = NaiveDate::parse_from_str(&self.transaction_date(), "%d/%m/%Y").unwrap();
        parsed_date > parsed_transaction_date
    }
    pub fn is_after(&self, date:&String) -> bool {
        let parsed_date = NaiveDate::parse_from_str(date, "%d/%m/%Y").unwrap();
        let parsed_transaction_date = NaiveDate::parse_from_str(&self.transaction_date(), "%d/%m/%Y").unwrap();
        parsed_date < parsed_transaction_date
    }

    pub fn is_greater(&self, amount:f64) -> bool {
        self.transaction_amount() >= amount
    }

    pub fn is_interval(&self) -> bool {
        self.transaction_interval() != "Không bao giờ"
    }

    pub fn from_transaction_data(transaction_data: TransactionData) -> Self {
        Self::new(transaction_data.id,transaction_data.source,transaction_data.description, transaction_data.amount, transaction_data.income, transaction_data.group, transaction_data.date, transaction_data.interval, transaction_data.enddate, transaction_data.done)
    }
}

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct TransactionData {
    pub id: u32,
    pub source: i32,
    pub description: String,
    pub amount: f64,
    pub income: bool,
    pub group: String,
    pub date: String,
    pub interval: String,
    pub enddate: String,
    pub done:bool
}
// ANCHOR: task_data